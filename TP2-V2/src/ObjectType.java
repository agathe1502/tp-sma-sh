public enum ObjectType {
    A, B, C;

    public ObjectType getOthersObjectType(){
        if (this.equals(A)) {
            return B;
        }
        return A;
    }
}
