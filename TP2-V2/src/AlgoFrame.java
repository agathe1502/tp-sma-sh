import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class AlgoFrame extends JFrame {

    private final ObjectGrid gridObjects;
    private boolean run = false;
    private final Environment environment;
    private final int nbIteration;

    public AlgoFrame(Environment environment, int nbIteration) {
        this.environment = environment;
        this.nbIteration = nbIteration;
        this.setBackground(Color.gray);
        this.setTitle("Algorithme de tri collectif");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(520,500);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setVisible(true);
        this.gridObjects = new ObjectGrid(environment.getGridObjects(), environment.getGridAgents(), environment.getGridPheromone(), environment);
        this.gridObjects.setVisible(true);
        this.add(this.gridObjects);
        this.addKeyListener(new KeyboardListener());
    }

    private class KeyboardListener implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE) {
                if(!run) {
                    run = true;
                    int iteration = 0;
                    while (iteration <= nbIteration) {
                        System.out.println("Iteration " + iteration);
                        environment.runIteration();

                        if(iteration % 10 == 0) {
                            gridObjects.paintImmediately(0,0, getWidth(), getHeight());
                            gridObjects.setNbIteration(iteration);
                        }
                        if (environment.getIndexCurrentAgentChosen() % environment.getAgents().size() == environment.getAgents().size() - 1) {
                            iteration++;
                            environment.evaporationPheromone();
                        }

                    }
                    gridObjects.repaint();
                }
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

}
