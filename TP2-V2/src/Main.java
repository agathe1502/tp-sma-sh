public class Main {

    public static void main(String[] args) {

        int x = 50;
        int y = 50;
        int nA = 200;
        int nB = 200;
        int nC = 100;
        int nAgents = 50;
        double kp = 0.1;
        double km = 0.3;
        int t = 100;
        int jump = 1;
        double error = 0.0;
        int ds = 1;
        double evaporation_rate = 0.3;
        int nbIterations = 100000;
        double intensity = 1.0;
        int nbIterationToCallHelp = 10;
        int nbTimesCallHelpToGiveUp = 5;
        Environment environment = new Environment(x, y, nA, nB, nC, nAgents, kp, km, t, jump, error, ds, evaporation_rate, intensity, nbIterationToCallHelp, nbTimesCallHelpToGiveUp);

        final AlgoFrame algoFrame = new AlgoFrame(environment, nbIterations);

        // -- Pour lancer dans la console
        environment.printGridObject();
        environment.printGridAgent();
//        environment.run(nbIterations);
//        environment.printGridObject();
//        environment.printAgentObject();

    }
}
