import javax.swing.*;
import java.awt.*;

public class ObjectGrid extends JPanel {
    private final ObjectType[][] gridObjects;
    private final Agent[][] gridAgents;
    private final double[][] gridPheromone;
    private final JLabel iterationLabel;
    private int nbIteration;
    private final Environment environment;

    public ObjectGrid(ObjectType[][] gridObjects, Agent[][] gridAgents, double[][] gridPheromone, Environment environment) {
        this.gridObjects = gridObjects;
        this.gridAgents = gridAgents;
        this.gridPheromone = gridPheromone;
        this.iterationLabel = new JLabel("", SwingConstants.CENTER);
        this.iterationLabel.setPreferredSize(new Dimension(600,50));
        this.iterationLabel.setForeground(Color.BLACK);
        this.add(iterationLabel);
        this.environment = environment;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        Dimension size = getSize();
        int w = size.width - 20;
        int h = size.height - 100;
        int nbLignes = gridObjects.length;
        int nbColonnes = gridObjects[0].length;

        setBounds(0, 0, 500, 500);
        final GridLayout gridLayout = new GridLayout(nbLignes, nbColonnes);
        this.setLayout(gridLayout);
        gridLayout.layoutContainer(this);

        for (int i = 0; i < nbLignes; i++) {
            for (int j = 0; j < nbColonnes; j++) {
                if (gridPheromone[i][j]!= 0) {
                    g2d.setColor(new Color(255, 255, 0, 150));
                    g2d.drawRect(10 + j*w/nbColonnes, 80+i*h/nbLignes, w/nbLignes, h/nbColonnes);
                    g2d.fillRect(10 + j*w/nbColonnes, 80+i*h/nbLignes, w/nbLignes, h/nbColonnes);
                }
                if (environment.getAgentAtPosition2(i, j, null).isPresent()) {
                    g2d.setColor(Color.blue);
                    g2d.fillRect(10 + j * w / nbColonnes, 80 + i * h / nbLignes, 5, 5);
                } else if (gridObjects[i][j] != null) {
                    if (gridObjects[i][j].equals(ObjectType.A)) {
                        g2d.setColor(Color.orange);
                    } else if (gridObjects[i][j].equals(ObjectType.B)){
                        g2d.setColor(Color.red);
                    } else {
                        g2d.setColor(Color.magenta);
                    }
                    g2d.fillRect(10 + j * w / nbColonnes, 80 + i * h / nbLignes, 5, 5);
                }
            }
        }
        iterationLabel.setText("Itération " + nbIteration);
    }

    public void setNbIteration(int nbIteration) {
        this.nbIteration = nbIteration;
    }
}
