import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Optional.empty;
import static java.util.Optional.of;

public class Agent {

    private final double kp;
    private final double km;
    private final int t;
    private final double error;
    private final String name;
    private final Queue<Optional<ObjectType>> memory;
    private final int nbIterationToCallHelp;
    private final int nbTimesCallHelpToGiveUp;
    private int x;
    private int y;
    private Optional<ObjectType> objectInHands = empty();
    private Optional<ObjectType> objectOnBox = empty();
    private Map<Direction, Boolean> adjacentBoxes;
    private boolean hasHelp;
    private int nbIterationSinceLastCall;
    private int nbTimesCallHelp = 0;


    public Agent(String name, double kp, double km, int t, double error, int nbIterationToCallHelp, int nbTimesCallHelpToGiveUp) {
        this.name = name;
        this.t = t;
        this.memory = new CircularFifoQueue<>(t);
        this.kp = kp;
        this.km = km;
        this.error = error;
        this.nbIterationToCallHelp = nbIterationToCallHelp;
        this.nbTimesCallHelpToGiveUp = nbTimesCallHelpToGiveUp;
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     * Indique si l'agent a de l'aide
     *
     * @return true s'il est aidé par un autre agent, false sinon
     */
    public boolean isHelped() {
        return hasHelp;
    }

    public int getNbIterationSinceLastCall() {
        return nbIterationSinceLastCall;
    }

    public void setNbIterationSinceLastCall(int nbIterationSinceLastCall) {
        this.nbIterationSinceLastCall = nbIterationSinceLastCall;
    }

    public int getNbTimesCallHelp() {
        return nbTimesCallHelp;
    }

    public void setNbTimesCallHelp(int nbTimesCallHelp) {
        this.nbTimesCallHelp = nbTimesCallHelp;
    }

    /**
     * Méthode de perception locale d'un agent
     * @param objectOnBox indique l'objet qu'il a sur sa case
     * @param adjacentBoxes indique les cases adjacentes avec les possibilités de déplacement
     * @param hasHelp indique s'il y a un agent sur la même case que lui
     */
    public void perception(Optional<ObjectType> objectOnBox, Map<Direction, Boolean> adjacentBoxes, boolean hasHelp) {
        this.objectOnBox = objectOnBox;
        this.adjacentBoxes = adjacentBoxes;
        this.hasHelp = hasHelp;
    }

    /**
     * Détermine l'action à réaliser selon sa perception :
     * - S'il a un objet dans les mains et qu'il peut le déposer selon une probabilité donnée : il dépose l'objet
     * - S'il y a un objet sur sa case et que la probabilité choisi de prendre l'objet :
     * a. S'il s'agit d'un objet de type C et qu'il n'a pas l'aide nécessaire, alors il appelle de l'aide
     * b. Sinon il prend l'objet
     * - Sinon s'il a deja demandé de l'aide :
     * a. S'il s'agit d'une itération où il devrait redemander de l'aide, il en redemande
     * b. Sinon s'il faut abandonner, il abandonne
     * c. Sinon il attend toujours de l'aide
     * - Sinon s'il a au moins une direction où se déplacer il se déplace selon une position aléatoire
     * - Sinon il ne fait aucune action car il ne peut pas se déplacer
     *
     * @return l'action à réaliser. S'il s'agit d'une action de déplacement, la direction est également indiquée
     */
    public Optional<TupleActionDirection> action() {
        if (nbTimesCallHelp > 0 ) {
            // L'agent a demandé de l'aide, donc soit il attend, soit il rappelle à l'aide, soit il abandonne
            if (nbIterationSinceLastCall % nbIterationToCallHelp == 0 && nbTimesCallHelp < nbTimesCallHelpToGiveUp) {
                // Il s'agit d'une itération où il faut appeler de l'aide
                return of(new TupleActionDirection(Action.CALL_HELP, null));
            } else if (nbIterationSinceLastCall % nbIterationToCallHelp == 0 && nbTimesCallHelp >= nbTimesCallHelpToGiveUp) {
                // Il s'agit d'une itération où il faut abandonner l'aide
                return of(new TupleActionDirection(Action.GIVE_UP, null));
            } else {
                // Il faut attendre de l'aide
                return of(new TupleActionDirection(Action.WAIT, null));
            }
        } else if (objectInHands.isEmpty() && objectOnBox.isPresent() && probPick()) {
            // L'agent n'a rien dans les mains et veut prendre un objet
            if (objectOnBox.get().equals(ObjectType.C) && !hasHelp) {
                // Si c'est un objet de type C et qu'il n'a pas d'aide, il faut qu'il appelle à l'aide
                return of(new TupleActionDirection(Action.CALL_HELP, null));
            }
            // Sinon, c'est qu'il a l'aide nécessaire, ou que ce n'est pas un type C, donc il peut prendre dans tous les cas
            return of(new TupleActionDirection(Action.PICK, null));
        } else if (objectInHands.isPresent() && objectOnBox.isEmpty() && probDrop()) {
            // L'agent a un objet dans les mains et peut le déposer
            // (pas besoin de vérifier le type de l'objet car il pourra poser si c'est un type C car il aura tjrs de l'aide)
            return of(new TupleActionDirection(Action.DROP, null));
        } else if (!canMove()) {
            // Il faudrait se déplacer mais aucune direction n'est possible
            return empty();
        }
        // On cherche aléatoirement une direction possible
        final List<Direction> collect = this.adjacentBoxes.entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        Random random = new Random();
        final int randomIndex = random.nextInt(collect.size());
        return of(new TupleActionDirection(Action.MOVE, collect.get(randomIndex)));
    }

    /**
     * Détermine si l'agent doit déposer selon une probabilité calculée
     *
     * @return true si la probabilité est respectée, false sinon
     */
    private boolean probDrop() {
        ObjectType objectType = getObjectInHands().get();
        // On compte le nombre d'objets de ce type dans la mémoire
        double f = (countInMemory(objectType) + countInMemory(objectType.getOthersObjectType()) * error) / this.t;
        Random random = new Random();
        final double randomDouble = random.nextDouble();
        return randomDouble <= Math.pow(f / (km + f), 2);
    }

    /**
     * Détermine si l'agent doit prendre selon une probabilité calculée
     *
     * @return true si la probabilité est respectée, false sinon
     */
    private boolean probPick() {
        ObjectType objectType = getObjectOnBox().get();
        // On compte le nombre d'objets de ce type dans la mémoire
        double f = (countInMemory(objectType) + countInMemory(objectType.getOthersObjectType()) * error) / this.t;
        Random random = new Random();
        final double randomDouble = random.nextDouble();
        return randomDouble <= Math.pow(kp / (kp + f), 2);
    }

    private double countInMemory(ObjectType objectType) {
        return this.memory.stream().filter(objectInQueue -> objectInQueue.isPresent() && objectInQueue.get().equals(objectType)).mapToDouble(objectInQueue -> 1.0).sum();
    }

    /**
     * Indique s'il y a une direction possible de déplacement
     *
     * @return true s'il peut bouger, false sinon
     */
    private boolean canMove() {
        return this.adjacentBoxes.values().stream().anyMatch(value -> value);
    }

    /**
     * Ajoute un type d'objet à sa mémoire. Si la taille limite est dépassée, le plus ancien objet est enlevé de la file
     *
     * @param objectTypeOptional le type d'objet à ajouter
     */
    public void addToMemory(Optional<ObjectType> objectTypeOptional) {
        this.memory.add(objectTypeOptional);
    }

    /**
     * Modifie l'objet qu'il a en main
     *
     * @param object l'objet qu'il a en main
     */
    public void setObject(Optional<ObjectType> object) {
        this.objectInHands = object;
    }

    /**
     * Récupère l'objet qu'il a en main
     *
     * @return l'objet qu'il a en main
     */
    public Optional<ObjectType> getObjectInHands() {
        return objectInHands;
    }

    /**
     * Indique l'objet qu'il y a sur la case
     *
     * @return l'objet sur la case
     */
    private Optional<ObjectType> getObjectOnBox() {
        return objectOnBox;
    }

    @Override
    public String toString() {
        return "Agent{" +
                "name='" + name + '\'' +
                '}';
    }

    public void setHasHelp(boolean hasHelp) {
        this.hasHelp = hasHelp;
    }
}
