import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class Environment {

    private final ObjectType[][] gridObjects;
    private final Agent[][] gridAgents;
    private final double[][] gridPheromone;
    private final List<Agent> agents;
    private final double kp;
    private final double km;
    private final int t;
    private final int jump;
    private final int nbTimesCallHelpToGiveUp;
    private final int nbIterationToCallHelp;
    private final double error;
    private final int ds;
    private final double evaporation_rate;
    private final double intensity;
    private int indexCurrentAgentChosen = -1;
    private Map<Agent, Agent> agentsObjectC = new HashMap<>();

    public Environment(int x, int y, int nA, int nB, int nC, int nAgents, double kp, double km, int t, int jump, double error, int ds, double evaporation_rate, double intensity, int nbIterationToCallHelp, int nbTimesCallHelpToGiveUp) {
        this.gridObjects = new ObjectType[x][y];
        this.gridAgents = new Agent[x][y];
        this.gridPheromone = new double[x][y];
        this.kp = kp;
        this.km = km;
        this.t = t;
        this.jump = jump;
        this.error = error;
        this.ds = ds;
        this.evaporation_rate = evaporation_rate;
        this.intensity = intensity;
        this.nbIterationToCallHelp = nbIterationToCallHelp;
        this.nbTimesCallHelpToGiveUp = nbTimesCallHelpToGiveUp;
        this.agents = new ArrayList<>();
        this.placeAgents(nAgents);
        this.placeObjects(nA, ObjectType.A);
        this.placeObjects(nB, ObjectType.B);
        this.placeObjects(nC, ObjectType.C);
    }

    /**
     * Initialise et place les agents dans la grille
     *
     * @param nAgents le nombre d'agents à initialiser
     */
    private void placeAgents(int nAgents) {
        Random random = new Random();
        int randX;
        int randY;
        for (int i = 0; i < nAgents; i++) {
            do {
                randX = random.nextInt(gridAgents.length);
                randY = random.nextInt(gridAgents[0].length);
            } while (getAgentAtPosition(randX, randY).isPresent());
            Agent agent = new Agent(Integer.toString(i), kp, km, t, error, nbIterationToCallHelp, nbTimesCallHelpToGiveUp);
            agent.setX(randX);
            agent.setY(randY);
            agents.add(agent);
            this.gridAgents[randX][randY] = agent;
        }
    }

    /**
     * Initialise et place les objets selon un type donné dans la grille
     *
     * @param n          le nombre d'objets à initialiser
     * @param objectType le type d'objet à initialiser
     */
    private void placeObjects(int n, ObjectType objectType) {
        Random random = new Random();
        int randX;
        int randY;
        for (int i = 0; i < n; i++) {
            do {
                randX = random.nextInt(gridObjects.length);
                randY = random.nextInt(gridObjects[0].length);
            } while (getObjectAtPosition(randX, randY).isPresent());
            this.gridObjects[randX][randY] = objectType;
        }
    }

    /**
     * Récupère l'agent à une position donnée
     *
     * @param x la position en x
     * @param y la position en y
     * @return un Optional d'un agent
     */
    public Optional<Agent> getAgentAtPosition(int x, int y) {
        return ofNullable(this.gridAgents[x][y]);
    }

    public Optional<Agent> getAgentAtPosition2(int x, int y, Agent agent) {
        final List<Agent> collect = agents.stream().filter(a -> a.getX() == x && a.getY() == y && (agent == null || !agent.equals(a))).collect(Collectors.toList());
        return ofNullable(collect.size() > 0 ? collect.get(0) : null);
    }


    /**
     * Récupère l'objet à une position donnée
     *
     * @param x la position en x
     * @param y la position en y
     * @return un Optional d'un objet
     */
    public Optional<ObjectType> getObjectAtPosition(int x, int y) {
        return ofNullable(this.gridObjects[x][y]);
    }

    /**
     * Lance le tri collectif. A chaque itération :
     * - un agent est choisi aléatoirement
     * - il execute sa perception locale
     * - il détermine une action
     * - l'environnement réalise cette action
     * - les grilles des objets et agents sont mises à jour
     */
    public void run(int nbIterations) {
        // Une itération se termine seulement une fois que les agents ont tous fait leur action
        int iteration = 0;
        while (iteration <= nbIterations) {
            runIteration();
            printGridObject();
            printGridAgent();
            printGridPheromone();
            if (indexCurrentAgentChosen % agents.size() == agents.size() - 1) {
                iteration++;
                evaporationPheromone();
            }
        }
    }

    /**
     * Lance une itération du tri collectif
     */
    public void runIteration() {
        // On choisi un agent selon la stratégie définie
        Agent agent = chooseAgent();
        // Perception
        agent.perception(getObjectAtPosition(agent.getX(), agent.getY()), getAdjacentBoxes(agent), getAgentOnSameBox(agent).isPresent());
        // Action
        Optional<TupleActionDirection> actionOptional = agent.action();
        if (actionOptional.isPresent()) {
            TupleActionDirection tupleActionDirection = actionOptional.get();
            Action action = tupleActionDirection.getAction();
            if (action.equals(Action.DROP)) {
                // On fait poser l'objet
                System.out.println("Agent " + agent.getName() + " : Poser objet");
                dropObject(agent);
            } else if (action.equals(Action.PICK)) {
                System.out.println("Agent " + agent.getName() + " : Prendre objet");
                // On fait prendre l'objet
                pickObject(agent);
            } else if (action.equals(Action.CALL_HELP)) {
                System.out.println("Agent " + agent.getName() + " : Appeler aide");
                callHelp(agent);
            } else if (action.equals(Action.GIVE_UP)) {
                System.out.println("Agent " + agent.getName() + " : Abandonner");
                giveUp(agent);
            } else if (action.equals(Action.MOVE)) {
                System.out.println("Agent " + agent.getName() + " : Se déplacer vers " + tupleActionDirection.getDirection());
                // On fait bouger l'agent
                moveAgent(agent, tupleActionDirection.getDirection());
                // On met à jour la mémoire avec le nouvel objet rencontré
                agent.addToMemory(ofNullable(gridObjects[agent.getX()][agent.getY()]));
            } else if (action.equals(Action.WAIT)) {
                // On incrémente son temps d'attente
                System.out.println("Agent " + agent.getName() + " : Attendre aide");
                agent.setNbIterationSinceLastCall(agent.getNbIterationSinceLastCall() + 1);
            }
        }
    }

    /**
     * Fait poser l'objet que l'agent a entre les mains
     *
     * @param agent l'agent
     */
    private void dropObject(Agent agent) {
        // Si c'est un objet de type C, il faut aussi faire poser l'objet à l'agent qui l'aide
        if (agent.getObjectInHands().get().equals(ObjectType.C)) {
            Agent secondAgent = agentsObjectC.get(agent);
            secondAgent.setObject(empty());
            agentsObjectC.remove(agent);
            agentsObjectC.remove(secondAgent);
        }
        this.gridObjects[agent.getX()][agent.getY()] = agent.getObjectInHands().get();
        agent.setObject(empty());
    }

    /**
     * Fait prendre l'objet de la case à l'agent
     *
     * @param agent l'agent
     */
    private void pickObject(Agent agent) {
        // Si c'est un objet de type C
        if (this.gridObjects[agent.getX()][agent.getY()].equals(ObjectType.C)) {
            Agent secondAgent = getAgentOnSameBox(agent).get();
            secondAgent.setObject(Optional.of(this.gridObjects[agent.getX()][agent.getY()]));
            agent.setObject(Optional.of(this.gridObjects[agent.getX()][agent.getY()]));
            agentsObjectC.put(agent, secondAgent);
            agentsObjectC.put(secondAgent, agent);
        } else {
            agent.setObject(Optional.of(this.gridObjects[agent.getX()][agent.getY()]));
        }
        giveUp(agent);
        this.gridObjects[agent.getX()][agent.getY()] = null;
    }

    private void callHelp(Agent agent) {
        int x = agent.getX();
        int y = agent.getY();
        this.gridPheromone[x][y] = 2 * intensity;
        for (int i = 1; i <= ds; i++) {
            double att = (i - 1) * intensity / ds;
            if (x + i < this.gridPheromone.length) {
                this.gridPheromone[x + i][y] = intensity - att;
                if (y + i < this.gridPheromone[0].length) {
                    this.gridPheromone[x + i][y + i] = intensity - att;
                }
                if (y - i >= 0) {
                    this.gridPheromone[x + i][y - i] = intensity - att;
                }
            }

            if (x - i >= 0) {
                this.gridPheromone[x - i][y] = intensity - att;
                if (y - i >= 0) {
                    this.gridPheromone[x - i][y - i] = intensity - att;
                }
            }

            if (y + i < this.gridPheromone[0].length) {
                this.gridPheromone[x][y + i] = intensity - att;
                if (x - i >= 0) {
                    this.gridPheromone[x - i][y + i] = intensity - att;
                }
            }

            if (y - i >= 0) {
                this.gridPheromone[x][y - i] = intensity - att;
            }
        }
        agent.setNbIterationSinceLastCall(1);
        agent.setNbTimesCallHelp(agent.getNbTimesCallHelp() + 1);
    }

    private void giveUp(Agent agent) {
        int x = agent.getX();
        int y = agent.getY();
        this.gridPheromone[x][y] = 0.0;
        for (int i = 1; i <= ds; i++) {
            if (x + i < this.gridPheromone.length) {
                this.gridPheromone[x + i][y] = 0.0;
                if (y + i < this.gridPheromone[0].length) {
                    this.gridPheromone[x + i][y + i] = 0.0;
                }
                if (y - i >= 0) {
                    this.gridPheromone[x + i][y - i] = 0.0;
                }
            }

            if (x - i >= 0) {
                this.gridPheromone[x - i][y] = 0.0;
                if (y - i >= 0) {
                    this.gridPheromone[x - i][y - i] = 0.0;
                }
            }

            if (y + i < this.gridPheromone[0].length) {
                this.gridPheromone[x][y + i] = 0.0;
                if (x - i >= 0) {
                    this.gridPheromone[x - i][y + i] = 0.0;
                }
            }

            if (y - i >= 0) {
                this.gridPheromone[x][y - i] = 0.0;
            }
        }
        agent.setNbIterationSinceLastCall(0);
        agent.setNbTimesCallHelp(0);
    }

    /**
     * Fait bouger l'agent selon une direction données
     *
     * @param agent     l'agent
     * @param direction la direction
     */
    private void moveAgent(Agent agent, Direction direction) {
        int oldX = agent.getX();
        int oldY = agent.getY();
        int newX = oldX;
        int newY = oldY;
        switch (direction) {
            case NORTH -> newX = oldX - jump;
            case EAST -> newY = oldY + jump;
            case WEST -> newY = oldY - jump;
            case SOUTH -> newX = oldX + jump;
            case NORTH_EAST -> {
                newX = oldX - jump;
                newY = oldY + jump;
            }
            case NORTH_WEST -> {
                newX = oldX - jump;
                newY = oldY - jump;
            }
            case SOUTH_EAST -> {
                newX = oldX + jump;
                newY = oldY + jump;
            }
            case SOUTH_WEST -> {
                newX = oldX + jump;
                newY = oldY - jump;
            }
        }
        // On déplace l'agent
        this.gridAgents[oldX][oldY] = null;
        agent.setX(newX);
        agent.setY(newY);
        if (this.gridAgents[newX][newY] == null) {
            this.gridAgents[newX][newY] = agent;
            agent.setHasHelp(false);
        } else {
            // Car s'il vaut pas null, alors il va aider qqn donc il ne faut pas enlever l'agent qu'il y avait de base sur la grille
            // Et il faut mettre à jour l'aide pour pas que qqn d'autre vienne d'ici la
            agent.setHasHelp(true);
            Agent secondAgent = this.gridAgents[newX][newY];
            secondAgent.setHasHelp(true);
        }

        // Si l'agent déplace un objet C : il faut aussi faire bouger l'autre agent qui l'aide
        if (agent.getObjectInHands().isPresent() && agent.getObjectInHands().get().equals(ObjectType.C)) {
            Agent secondAgent = agentsObjectC.get(agent);
            secondAgent.setX(newX);
            secondAgent.setY(newY);
        }
    }

    /**
     * Récupère les informations sur les possibilités de déplacement d'un agent.
     * Il peut se déplacer seulement s'il n'est pas face à un mur et qu'il n'y a pas déjà un agent sur la case.
     * Il y a une exception, il peut se déplacer sur une case avec un agent, seulement si cette case a un objet de type C,
     * et que l'agent de la case n'est pas déjà aidé par un autre agent.
     *
     * @param agent l'agent
     * @return la map des déplacements possibles
     */
    private Map<Direction, Boolean> getAdjacentBoxes(Agent agent) {
        Map<Direction, Boolean> mapAdjacentBoxes = new HashMap<>();
        final ArrayList<Direction> maxPheromoneDirection = new ArrayList<>();
        final double[] maxPheromone = {0.0};
        List<Direction> listDirection = Direction.getValues();
        listDirection.forEach(direction -> {
            AtomicInteger posX = new AtomicInteger(agent.getX());
            AtomicInteger posY = new AtomicInteger(agent.getY());
            switch (direction) {
                case WEST -> posY.addAndGet(-jump);
                case SOUTH_WEST -> {
                    posX.addAndGet(jump);
                    posY.addAndGet(-jump);
                }
                case SOUTH -> posX.addAndGet(jump);
                case SOUTH_EAST -> {
                    posX.addAndGet(jump);
                    posY.addAndGet(jump);
                }
                case EAST -> posY.addAndGet(jump);
                case NORTH_EAST -> {
                    posX.addAndGet(-jump);
                    posY.addAndGet(jump);
                }
                case NORTH -> posX.addAndGet(-jump);
                case NORTH_WEST -> {
                    posX.addAndGet(-jump);
                    posY.addAndGet(-jump);
                }
            }

            if (posY.get() >= 0 && posY.get() < this.gridAgents[0].length && posX.get() >= 0 &&
                    posX.get() < this.gridAgents.length &&
                    maxPheromone[0] <= this.gridPheromone[posX.get()][posY.get()] && this.gridPheromone[posX.get()][posY.get()] != 0.0 &&
                    (agent.getObjectInHands().isEmpty() || !agent.getObjectInHands().get().equals(ObjectType.C))) {
                maxPheromone[0] = this.gridPheromone[posX.get()][posY.get()];
                maxPheromoneDirection.add(direction);
            }

            mapAdjacentBoxes.put(direction,
                    posX.get() >= 0 && posX.get() < this.gridAgents.length &&
                            posY.get() >= 0 && posY.get() < this.gridAgents[0].length &&
                            (this.getAgentAtPosition(posX.get(), posY.get()).isEmpty() || (this.getObjectAtPosition(posX.get(), posY.get()).isPresent() && this.getObjectAtPosition(posX.get(), posY.get()).get().equals(ObjectType.C) && !this.getAgentAtPosition(posX.get(), posY.get()).get().isHelped()))
            );
        });

        if (maxPheromoneDirection.size() != 0) {
            listDirection.forEach(direction -> {
                mapAdjacentBoxes.put(direction, mapAdjacentBoxes.get(direction) && maxPheromoneDirection.contains(direction));
            });
        }
        return mapAdjacentBoxes;
    }

    private Optional<Agent> getAgentOnSameBox(Agent agent) {
        final List<Agent> collect = agents.stream().filter(a -> a.getX() == agent.getX() && a.getY() == agent.getY() && !agent.equals(a)).collect(Collectors.toList());
        return ofNullable(collect.size() > 0 ? collect.get(0) : null);
    }

    /**
     * Permet de choisir l'agent selon la stratégie choisie. Ici le choix est fait en alternant dans l'ordre les agents
     *
     * @return l'agent choisi
     */
    private Agent chooseAgent() {
        indexCurrentAgentChosen++;
        return agents.get(indexCurrentAgentChosen % agents.size());
    }

    /**
     * Méthode pour faire évaporer les phéromones selon un taux d'évaporation donné
     */
    public void evaporationPheromone() {
        for (int i = 0; i < gridPheromone.length; i++) {
            for (int j = 0; j < gridPheromone[0].length; j++) {
                this.gridPheromone[i][j] = Math.round(this.gridPheromone[i][j] * (1 - evaporation_rate) * 1000) / 1000;
            }
        }
    }

    /**
     * Affiche la grille des objets
     */
    public void printGridObject() {
        System.out.println("\nGrille Object : ------");
        for (int i = 0; i < gridObjects.length; i++) {
            for (int j = 0; j < gridObjects[0].length; j++) {
                System.out.print("[");
                if (getObjectAtPosition(i, j).isPresent()) {
                    System.out.print(getObjectAtPosition(i, j).get());
                } else {
                    System.out.print(" ");
                }
                System.out.print("]");

            }
            System.out.print('\n');
        }
    }

    /**
     * Affiche la grille des agents
     */
    public void printGridAgent() {
        System.out.println("\nGrille Agent : ------");
        for (int i = 0; i < gridAgents.length; i++) {
            for (int j = 0; j < gridAgents[0].length; j++) {
                System.out.print("[");
                if (getAgentAtPosition(i, j).isPresent()) {
                    System.out.print(getAgentAtPosition(i, j).get().getName());
                } else {
                    System.out.print(" ");
                }
                System.out.print("]");

            }
            System.out.print('\n');
        }
    }

    /**
     * Affiche la grille des phéromones
     */
    public void printGridPheromone() {
        final DecimalFormat df = new DecimalFormat("0.00");
        System.out.println("\nGrille Phéromone : ------");
        for (int i = 0; i < gridPheromone.length; i++) {
            for (int j = 0; j < gridPheromone[0].length; j++) {
                System.out.print("[");
                System.out.print(df.format(gridPheromone[i][j]));
                System.out.print("]");
            }
            System.out.print('\n');
        }
    }

    public ObjectType[][] getGridObjects() {
        return gridObjects;
    }

    public Agent[][] getGridAgents() {
        return gridAgents;
    }

    public double[][] getGridPheromone() {
        return gridPheromone;
    }

    public List<Agent> getAgents() {
        return agents;
    }

    public int getIndexCurrentAgentChosen() {
        return indexCurrentAgentChosen;
    }
}
