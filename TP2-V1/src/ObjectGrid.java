import javax.swing.*;
import java.awt.*;

public class ObjectGrid extends JPanel {
    private final ObjectType[][] gridObjects;
    private final Agent[][] gridAgents;

    public ObjectGrid(ObjectType[][] gridObjects, Agent[][] gridAgents) {
        this.gridObjects = gridObjects;
        this.gridAgents = gridAgents;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        Dimension size = getSize();
        int w = size.width - 20;
        int h = size.height - 100;
        int nbLignes = gridObjects.length;
        int nbColonnes = gridObjects[0].length;

        setBounds(0, 0, 500, 500);
        final GridLayout gridLayout = new GridLayout(nbLignes, nbColonnes);
        this.setLayout(gridLayout);
        gridLayout.layoutContainer(this);

        for (int i = 0; i < nbLignes; i++) {
            for (int j = 0; j < nbColonnes; j++) {
                if (gridAgents[i][j] != null) {
                    g2d.setColor(Color.blue);
                    g2d.fillRect(10 + j * w / nbColonnes, 80 + i * h / nbLignes, 5, 5);
                } else if (gridObjects[i][j] != null) {
                    if (gridObjects[i][j].equals(ObjectType.A)) {
                        g2d.setColor(Color.orange);
                    } else {
                        g2d.setColor(Color.red);
                    }
                    g2d.fillRect(10 + j * w / nbColonnes, 80 + i * h / nbLignes, 5, 5);
                }
            }
        }
    }
}
