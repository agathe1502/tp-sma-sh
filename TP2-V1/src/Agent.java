import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Optional.empty;
import static java.util.Optional.of;

public class Agent {

    private final double kp;
    private final double km;
    private final int t;
    private final double error;
    private int x;
    private int y;
    private final String name;
    private final Queue<Optional<ObjectType>> memory;
    private Optional<ObjectType> objectInHands = empty();
    private Optional<ObjectType> objectOnBox = empty();
    private Map<Direction, Boolean> adjacentBoxes;


    public Agent(String name, double kp, double km, int t, double error) {
        this.name = name;
        this.t = t;
        this.memory = new CircularFifoQueue<>(t);
        this.kp = kp;
        this.km = km;
        this.error = error;
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void perception(Optional<ObjectType> objectOnBox, Map<Direction, Boolean> adjacentBoxes) {
        this.objectOnBox = objectOnBox;
        this.adjacentBoxes = adjacentBoxes;
    }

    /**
     * Détermine l'action à réaliser selon sa perception :
     * - S'il a un objet dans les mains et qu'il peut le déposer selon une probabilité donnée : il dépose l'objet
     * - S'il y a un objet sur sa case et qu'il peut le prendre selon une probabilité donnée : il prend l'objet
     * - Sinon s'il a au moins une direction où se déplacer il se déplace selon une position aléatoire
     * - Sinon il ne fait aucune action car il ne peut pas se déplacer
     *
     * @return l'action à réaliser. S'il s'agit d'une action de déplacement, la direction est également indiquée
     */
    public Optional<TupleActionDirection> action() {
        if (objectInHands.isPresent() && objectOnBox.isEmpty() && probDrop()) {
            // L'agent a un objet dans les mains et peut le déposer
            return of(new TupleActionDirection(Action.DROP, null));
        } else if (objectInHands.isEmpty() && objectOnBox.isPresent() && probPick()) {
            // L'agent n'a rien dans les mains et on peut prendre un objet
            return of(new TupleActionDirection(Action.PICK, null));
        } else if (!canMove()) {
            // Il faudrait se déplacer mais aucune direction n'est possible
            return empty();
        }
        // On cherche aléatoirement une direction possible
        final List<Direction> collect = this.adjacentBoxes.entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        Random random = new Random();
        final int randomIndex = random.nextInt(collect.size());
        return of(new TupleActionDirection(Action.MOVE, collect.get(randomIndex)));
    }

    /**
     * Détermine si l'agent doit déposer selon une probabilité calculée
     *
     * @return true si la probabilité est respectée, false sinon
     */
    private boolean probDrop() {
        ObjectType objectType = getObjectInHands().get();
        // On compte le nombre d'objets de ce type dans la mémoire
        double f = (countInMemory(objectType) + countInMemory(objectType.getOthersObjectType()) * error) / this.t;
        Random random = new Random();
        final double randomDouble = random.nextDouble();
        return randomDouble <= Math.pow(f / (km + f), 2);
    }

    /**
     * Détermine si l'agent doit prendre selon une probabilité calculée
     *
     * @return true si la probabilité est respectée, false sinon
     */
    private boolean probPick() {
        ObjectType objectType = getObjectOnBox().get();
        // On compte le nombre d'objets de ce type dans la mémoire
        double f = (countInMemory(objectType) + countInMemory(objectType.getOthersObjectType()) * error) / this.t;
        Random random = new Random();
        final double randomDouble = random.nextDouble();
        return randomDouble <= Math.pow(kp / (kp + f), 2);
    }

    private double countInMemory(ObjectType objectType) {
        return this.memory.stream().filter(objectInQueue -> objectInQueue.isPresent() && objectInQueue.get().equals(objectType)).mapToDouble(objectInQueue -> 1.0).sum();
    }

    /**
     * Indique s'il y a une direction possible de déplacement
     *
     * @return true s'il peut bouger, false sinon
     */
    private boolean canMove() {
        return this.adjacentBoxes.values().stream().anyMatch(value -> value);
    }

    /**
     * Ajoute un type d'objet à sa mémoire. Si la taille limite est dépassée, le plus ancien objet est enlevé de la file
     *
     * @param objectTypeOptional le type d'objet à ajouter
     */
    public void addToMemory(Optional<ObjectType> objectTypeOptional) {
        this.memory.add(objectTypeOptional);
    }

    /**
     * Indique l'objet qu'il a en mains
     *
     * @param object l'objet qu'il a en main
     */
    public void setObject(Optional<ObjectType> object) {
        this.objectInHands = object;
    }

    /**
     * Récupère l'objet qu'il a en mains
     *
     * @return l'objet qu'il a en main
     */
    public Optional<ObjectType> getObjectInHands() {
        return objectInHands;
    }

    private Optional<ObjectType> getObjectOnBox() {
        return objectOnBox;
    }
}
