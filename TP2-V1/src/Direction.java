import java.util.List;

public enum Direction {
    NORTH, SOUTH, WEST, EAST, NORTH_WEST, SOUTH_WEST, NORTH_EAST, SOUTH_EAST;

    private static final List<Direction> VALUES = List.of(values());

    public static List<Direction> getValues() {
        return VALUES;
    }
}
