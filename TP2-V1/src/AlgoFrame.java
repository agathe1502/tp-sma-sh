import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class AlgoFrame extends JFrame {

    private final ObjectGrid gridObjects;
    private boolean run = false;
    private final Environment environment;
    private final int nbIteration;

    public AlgoFrame(Environment environment, int nbIteration) {
        this.environment = environment;
        this.nbIteration = nbIteration;
        this.setBackground(Color.gray);
        this.setTitle("Algorithme de tri collectif");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(700,600);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setVisible(true);
        this.gridObjects = new ObjectGrid(environment.getGridObjects(), environment.getGridAgents());
        this.gridObjects.setVisible(true);
        this.add(this.gridObjects);
        this.addKeyListener(new KeyboardListener());
    }

    private class KeyboardListener implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                if(!run) {
                    run = true;
                    for (int i = 0; i < nbIteration; i++) {
                        environment.runIteration();

                        if(i % 1000 == 0) {
                            gridObjects.paintImmediately(0,0, getWidth(), getHeight());
                        }

                    }
                    gridObjects.repaint();
                }
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }

}
