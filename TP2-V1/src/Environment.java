import java.util.*;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class Environment {

    private final ObjectType[][] gridObjects;
    private final Agent[][] gridAgents;
    private final List<Agent> agents;
    private final double kp;
    private final double km;
    private final int t;
    private final int jump;
    private int indexCurrentAgentChosen = -1;
    private final double error;

    public Environment(int x, int y, int nA, int nB, int nAgents, double kp, double km, int t, int jump, double error) {
        this.gridObjects = new ObjectType[x][y];
        this.gridAgents = new Agent[x][y];
        this.kp = kp;
        this.km = km;
        this.t = t;
        this.jump = jump;
        this.error = error;
        this.agents = new ArrayList<>();
        this.placeAgents(nAgents);
        this.placeObjects(nA, ObjectType.A);
        this.placeObjects(nB, ObjectType.B);
    }

    /**
     * Initialise et place les agents dans la grille
     *
     * @param nAgents le nombre d'agents à initialiser
     */
    private void placeAgents(int nAgents) {
        Random random = new Random();
        int randX;
        int randY;
        for (int i = 0; i < nAgents; i++) {
            do {
                randX = random.nextInt(gridAgents.length);
                randY = random.nextInt(gridAgents[0].length);
            } while (getAgentAtPosition(randX, randY).isPresent());
            Agent agent = new Agent(Integer.toString(i), kp, km, t, error);
            agent.setX(randX);
            agent.setY(randY);
            agents.add(agent);
            this.gridAgents[randX][randY] = agent;
        }
    }

    /**
     * Initialise et place les objets selon un type donné dans la grille
     *
     * @param n          le nombre d'objets à initialiser
     * @param objectType le type d'objet à initialiser
     */
    private void placeObjects(int n, ObjectType objectType) {
        Random random = new Random();
        int randX;
        int randY;
        for (int i = 0; i < n; i++) {
            do {
                randX = random.nextInt(gridObjects.length);
                randY = random.nextInt(gridObjects[0].length);
            } while (getObjectAtPosition(randX, randY).isPresent());
            this.gridObjects[randX][randY] = objectType;
        }
    }

    /**
     * Récupère l'agent à une position donnée
     *
     * @param x la position en x
     * @param y la position en y
     * @return un Optional d'un agent
     */
    public Optional<Agent> getAgentAtPosition(int x, int y) {
        return ofNullable(this.gridAgents[x][y]);
    }


    /**
     * Récupère l'objet à une position donnée
     *
     * @param x la position en x
     * @param y la position en y
     * @return un Optional d'un objet
     */
    public Optional<ObjectType> getObjectAtPosition(int x, int y) {
        return ofNullable(this.gridObjects[x][y]);
    }

    /**
     * Lance le tri collectif. A chaque itération :
     * - un agent est choisi aléatoirement
     * - il execute sa perception locale
     * - il détermine une action
     * - l'environnement réalise cette action
     * - les grilles des objets et agents sont mises à jour
     */
    public void run() {
        int step = 0;
        while (step <= 1000000) {
            runIteration();
            step ++;
        }
    }

    public void runIteration() {
        // On choisi un agent selon la stratégie définie
        Agent agent = chooseAgent();
        // Perception
        agent.perception(getObjectAtPosition(agent.getX(), agent.getY()), getAdjacentBoxes(agent));
        // Action
        Optional<TupleActionDirection> actionOptional = agent.action();
        if (actionOptional.isPresent()) {
            TupleActionDirection tupleActionDirection = actionOptional.get();
            Action action = tupleActionDirection.getAction();
            if (action.equals(Action.DROP)) {
                // On fait poser l'objet
                System.out.println("Agent " + agent.getName() + " : Poser objet");
                dropObject(agent);
            } else if (action.equals(Action.PICK)) {
                System.out.println("Agent " + agent.getName() + " : Prendre objet");
                // On fait prendre l'objet
                pickObject(agent);
            } else {
                System.out.println("Agent " + agent.getName() + " : Se déplacer vers " + tupleActionDirection.getDirection());
                // On fait bouger l'agent
                moveAgent(agent, tupleActionDirection.getDirection());
                // On met à jour la mémoire avec le nouvel objet rencontré
                agent.addToMemory(ofNullable(gridObjects[agent.getX()][agent.getY()]));
            }
        }
    }

    /**
     * Fait poser l'objet que l'agent a entre les mains
     *
     * @param agent l'agent
     */
    private void dropObject(Agent agent) {
        this.gridObjects[agent.getX()][agent.getY()] = agent.getObjectInHands().get();
        agent.setObject(empty());
    }

    /**
     * Fait prendre l'objet de la case à l'agent
     *
     * @param agent l'agent
     */
    private void pickObject(Agent agent) {
        agent.setObject(Optional.of(this.gridObjects[agent.getX()][agent.getY()]));
        this.gridObjects[agent.getX()][agent.getY()] = null;
    }

    /**
     * Fait bouger l'agent selon une direction données
     *
     * @param agent     l'agent
     * @param direction la direction
     */
    private void moveAgent(Agent agent, Direction direction) {
        int oldX = agent.getX();
        int oldY = agent.getY();
        int newX = oldX;
        int newY = oldY;
        switch (direction) {
            case NORTH -> newX = oldX - jump;
            case EAST -> newY = oldY + jump;
            case WEST -> newY = oldY - jump;
            case SOUTH -> newX = oldX + jump;
            case NORTH_EAST -> {
                newX = oldX - jump;
                newY = oldY + jump;
            }
            case NORTH_WEST -> {
                newX = oldX - jump;
                newY = oldY - jump;
            }
            case SOUTH_EAST -> {
                newX = oldX + jump;
                newY = oldY + jump;
            }
            case SOUTH_WEST -> {
                newX = oldX + jump;
                newY = oldY - jump;
            }
        }
        // On déplace l'agent
        this.gridAgents[oldX][oldY] = null;
        agent.setX(newX);
        agent.setY(newY);
        this.gridAgents[newX][newY] = agent;
    }

    /**
     * Récupère les informations sur les possibilités de déplacement d'un agent.
     * Il peut se déplacer seulement s'il n'est pas face à un mur et qu'il n'y a pas déjà un agent sur la case
     *
     * @param agent l'agent
     * @return la map des déplacements possibles
     */
    private Map<Direction, Boolean> getAdjacentBoxes(Agent agent) {
        Map<Direction, Boolean> mapAdjacentBoxes = new HashMap<>();
        List<Direction> listDirection = Direction.getValues();
        listDirection.forEach(direction -> {
            switch (direction) {
                case WEST -> mapAdjacentBoxes.put(Direction.WEST, agent.getY() - jump >= 0 && this.getAgentAtPosition(agent.getX(), agent.getY() - jump).isEmpty());
                case SOUTH_WEST -> mapAdjacentBoxes.put(Direction.SOUTH_WEST, agent.getY() - jump >= 0 && agent.getX() + jump < this.gridAgents.length && this.getAgentAtPosition(agent.getX() + jump, agent.getY() - jump).isEmpty());
                case SOUTH -> mapAdjacentBoxes.put(Direction.SOUTH, agent.getX() + jump < this.gridAgents.length && this.getAgentAtPosition(agent.getX() + jump, agent.getY()).isEmpty());
                case SOUTH_EAST -> mapAdjacentBoxes.put(Direction.SOUTH_EAST, agent.getX() + jump < this.gridAgents.length && agent.getY() + jump < this.gridAgents[0].length && this.getAgentAtPosition(agent.getX() + jump, agent.getY() + jump).isEmpty());
                case EAST -> mapAdjacentBoxes.put(Direction.EAST, agent.getY() + jump < this.gridAgents[0].length && this.getAgentAtPosition(agent.getX(), agent.getY() + jump).isEmpty());
                case NORTH_EAST -> mapAdjacentBoxes.put(Direction.NORTH_EAST, agent.getX() - jump >= 0 && agent.getY() + jump < this.gridAgents[0].length && this.getAgentAtPosition(agent.getX() - jump, agent.getY() + jump).isEmpty());
                case NORTH -> mapAdjacentBoxes.put(Direction.NORTH, agent.getX() - jump >= 0 && this.getAgentAtPosition(agent.getX() - jump, agent.getY()).isEmpty());
                case NORTH_WEST -> mapAdjacentBoxes.put(Direction.NORTH_WEST, agent.getX() - jump >= 0 && agent.getY() - jump >= 0 && this.getAgentAtPosition(agent.getX() - jump, agent.getY() - jump).isEmpty());
            }
        });
        return mapAdjacentBoxes;
    }

    /**
     * Permet de choisir l'agent selon la stratégie choisie. Ici le choix est fait en alternant dans l'ordre les agents
     *
     * @return l'agent choisi
     */
    private Agent chooseAgent() {
        indexCurrentAgentChosen++;
        return agents.get(indexCurrentAgentChosen % agents.size());
    }

    /**
     * Affiche la grille des objets
     */
    public void printGridObject() {
        System.out.println("\nGrille Object : ------");
        for (int i = 0; i < gridObjects.length; i++) {
            for (int j = 0; j < gridObjects[0].length; j++) {
                System.out.print("[");
                if (getObjectAtPosition(i, j).isPresent()) {
                    System.out.print(getObjectAtPosition(i, j).get());
                } else {
                    System.out.print(" ");
                }
                System.out.print("]");

            }
            System.out.print('\n');
        }
    }

    /**
     * Affiche la grille des agents
     */
    public void printAgentObject() {
        System.out.println("\nGrille Agent : ------");
        for (int i = 0; i < gridAgents.length; i++) {
            for (int j = 0; j < gridAgents[0].length; j++) {
                System.out.print("[");
                if (getAgentAtPosition(i, j).isPresent()) {
                    System.out.print(getAgentAtPosition(i, j).get().getName());
                } else {
                    System.out.print(" ");
                }
                System.out.print("]");

            }
            System.out.print('\n');
        }
    }

    public ObjectType[][] getGridObjects() {
        return gridObjects;
    }

    public Agent[][] getGridAgents() {
        return gridAgents;
    }
}
