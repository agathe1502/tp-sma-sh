public enum ObjectType {
    A, B;

    public ObjectType getOthersObjectType(){
        if (this.equals(A)) {
            return B;
        }
        return A;
    }
}
