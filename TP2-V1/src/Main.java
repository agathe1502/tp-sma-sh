public class Main {

    public static void main(String[] args) {

        int x = 50;
        int y = 50;
        int nA = 200;
        int nB = 200;
        int nAgents = 20;
        double kp = 0.1;
        double km = 0.3;
        int t = 10;
        int jump = 1;
        double error = 0.1;
        int nbIterations = 1000000;
        Environment environment = new Environment(x, y, nA, nB, nAgents, kp, km, t, jump, error);

        final AlgoFrame algoFrame = new AlgoFrame(environment, nbIterations);

        // -- Pour lancer dans la console
//        environment.printGridObject();
//        environment.printAgentObject();
//        environment.run();
//        environment.printGridObject();
//        environment.printAgentObject();

    }
}
