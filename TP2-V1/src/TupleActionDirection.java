public class TupleActionDirection {
  public final Action action;
  public final Direction direction;

  public TupleActionDirection(Action action, Direction direction) {
    this.action = action;
    this.direction = direction;
  }

  public Action getAction() {
    return action;
  }

  public Direction getDirection() {
    return direction;
  }
}